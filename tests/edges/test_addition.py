import unittest
from typing import Optional, Tuple, List

import jax.numpy as np
from autograd.edges import sum_op
from autograd.nodes.tensor import Tensor
from jax import value_and_grad
from jax.test_util import check_eq


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.DIMENSIONALITY = 5
        self.SCALING_FACTOR = 2

        def sum(val: np.ndarray, axis: Optional[int]):
            return val.sum(axis=axis)

        self.check = sum
        self.vector_to_calc = self.SCALING_FACTOR * np.ones(shape=[self.DIMENSIONALITY])
        self.matrix_to_calc = self.SCALING_FACTOR * np.ones(shape=[2, self.DIMENSIONALITY])

    def sum_setup(self, data, data_as_tensor, axis=None) -> List:
        def _expected():
            v_g_grad = value_and_grad(self.check)
            val, grad = v_g_grad(data, axis)
            return val, grad

        def _actual():
            returned_tensor = sum_op(data_as_tensor, axis=axis)
            _, grad_fn = returned_tensor.connected_to[0]
            grad = grad_fn(gradient=1)
            return returned_tensor.data, grad

        expected: Tuple = _expected()
        actual: Tuple = _actual()
        return [*expected, *actual]


if __name__ == '__main__':
    unittest.main()
