import jax.numpy as np
from typing import Optional, Union, Tuple, List, NamedTuple, Callable

Shape = Union[Tuple[int], List[int]]


class Connection(NamedTuple):
    """
    container class to tie our tensors to the backwards gradient
    along which we iterate
    """

    tensor: "Tensor"
    gradient_op: Callable


class Tensor:
    def __init__(
        self,
        data: Optional[np.ndarray] = None,
        connected_to: Optional[List[Connection]] = None,
        shape: Optional[Shape] = None,
        requires_grad: bool = False,
        dtype: np.dtype = np.float32,
        tensor_name: str = "",
    ) -> None:

        self.data: np.ndarray
        self.shape: Shape
        self.connected_to: Optional[List[Connection]]

        if data is not None:
            self.data = data
            self.shape = data.shape
        else:
            self.data = np.empty(shape=self.shape, dtype=dtype)
            self.shape = shape

        self.requires_grad: bool = requires_grad
        self.dtype: np.dtype = dtype
        self.name = tensor_name

        if connected_to is None:
            self.connected_to = []
        else:
            self.connected_to = connected_to

        self.grad = self.reset_grad()

    def calculate_gradient(self, gradient: "Tensor"):
        """
        Uses
        :param gradient:
        :return:
        """
        assert (
            self.requires_grad
        ), "Called calculate_gradient on something that doesn't require grad"

        for connection, func in self.connected_to:
            grad = func.grad_fn(self, gradient.data)
            connection.backward(grad)

    def reset_grad(self) -> Optional[np.ndarray]:
        if self.requires_grad:
            return np.zeros_like(self.data)
        return None

    def __repr__(self) -> str:
        return (
            f"(name: {self.name}, "
            f"data: {self.data}, "
            f"shape: {self.shape}, "
            f"requires_grad: {self.requires_grad}, "
            f"dtype: {self.dtype})"
        )

    def _cast(self, dtype: np.dtype) -> "Tensor":
        return Tensor(
            data=np.array(object=self.data, copy=False),
            requires_grad=self.requires_grad,
            dtype=dtype,
        )

