import unittest
from typing import Optional, Tuple

import jax.numpy as np
from autograd.code.simple.simple_tensor import sum_op, Tensor
from jax import value_and_grad
from jax.test_util import check_eq


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.DIMENSIONALITY = 5
        self.SCALING_FACTOR = 2

        def sum(val: np.ndarray, axis: Optional[int]):
            return val.sum(axis=axis)

        self.check = sum

    def sum_setup(self, data, data_as_tensor, axis=None) -> Tuple:
        def _expected():
            v_g_grad = value_and_grad(self.check)
            val, grad = v_g_grad(data, axis)
            return val, grad

        def _actual():
            returned_tensor = sum_op(data_as_tensor, axis=axis)
            _, grad_fn = returned_tensor.connected_to[0]
            grad = grad_fn(scale=1)
            return returned_tensor.data, grad

        expected: Tuple = _expected()
        actual: Tuple = _actual()
        return (*expected, *actual)

    def test_sum_vector_wrt_scalar(self):
        data = self.SCALING_FACTOR * np.ones(shape=[self.DIMENSIONALITY])
        my_tensor = Tensor(data=data, requires_grad=True)

        expected_val, expected_grad, actual_val, actual_grad = self.sum_setup(
            data, my_tensor
        )
        check_eq(actual_grad, expected_grad)
        self.assertEqual(float(actual_val), float(expected_val))

    def test_sum_matrix_wrt_scalar(self):
        data = self.SCALING_FACTOR * np.ones(shape=[2, self.DIMENSIONALITY])
        my_tensor = Tensor(data=data, requires_grad=True)

        expected_val, expected_grad, actual_val, actual_grad = self.sum_setup(
            data, my_tensor, None
        )
        check_eq(actual_grad, expected_grad)
        self.assertEqual(float(actual_val), float(expected_val))

    def test_sum_vector_wrt_vector(self):
        data = self.SCALING_FACTOR * np.ones(shape=[self.DIMENSIONALITY])
        my_tensor = Tensor(data=data, requires_grad=True)
        AXIS = 0
        expected_val, expected_grad, actual_val, actual_grad = self.sum_setup(
            data, my_tensor, AXIS
        )
        check_eq(actual_grad, expected_grad)
        self.assertEqual(float(actual_val), float(expected_val))


if __name__ == "__main__":
    unittest.main()

