# Objective

1) Simple POC before I dive in and abstract everything out

2) Add some sanity checks via tests

3) Rock and roll

# Notes

1) PyTorch seems to use the total derivative instead of partials. I guess we're doing totals, then.

# Conclusion:

Dope! Looks the same as Jax so far. One thing I'd like is to be able to differentiate across arbitrary tensors but it looks like jax and PyTorch only support differentiation of a scalar so I have nothing to compare against? 

- time to abstract out
