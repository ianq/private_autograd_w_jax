# Connections / Edges

- connect our graph nodes to one another

- describes the mathematical operations between one another

## Implementation Notes

1) All edges dealing with Tensors MUST take in the Tensor type

2) MUST output Tensors

## Useful notes

1) Imagine you're moving backwards through a graph from some final node (the output)

2) You will want to consider each input to the graph independently. I.e iterate all incoming notes "freezing" all the others and take the derivative that way.