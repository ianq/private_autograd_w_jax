import jax.numpy as np
from typing import Optional, Callable
from autograd.nodes.tensor import Tensor, Connection


def sum_op(t: Tensor, axis: Optional[int] = None) -> Tensor:
    """
    Define the summation operator and allow the user to specify an axis

    When axis=None, reduces to a scalar else, we reduce along a dimension
        Note: the numpy interface allows tuple axis specification but
        we ignore this case

    :param t:
        our tensor to reduce
    :param axis:
        along which axis to do the contraction
    :return:
        Tensor such that return_tensor.shape < t.shape

    Note:
        https://en.wikipedia.org/wiki/Matrix_calculus

        ^ HELLA useful
    """
    data = t.data
    value = np.sum(data, axis=axis)
    if t.requires_grad:
        grad: Callable

        def grad(gradient: Tensor) -> Tensor:
            """
            Returns a scalar

            :param gradient:
            :return:
            """
            return np.ones_like(t.data) * gradient

        connections = [Connection(t, grad)]
    else:
        connections = []

    return Tensor(data=value, requires_grad=t.requires_grad, connected_to=connections)

