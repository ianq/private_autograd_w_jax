import jax.numpy as np
from typing import Optional, Callable
from autograd.nodes.tensor import Tensor, Connection


def matrix_mult_op(t1: Tensor, t2: Tensor) -> Tensor:
    """

    Args:
        t1:     t1 = (m, n)
        t2:     t2 = (n, p)

    Returns:
        new tensor of size (m, p)
        with gradient

    """

    data = t1.data * t2.data
    connections = []
    if t1.requires_grad:
        grad: Callable

        def grad(gradient: Tensor) -> Tensor:
            """
            Hold t2 constant and take the derivative. We get back 1, so we multiply by t2
            Args:
                gradient:
                    tensor data

            Returns:
            """
            return np.matmul(gradient, t2.data.T)

        connections = [Connection(t1, grad)]
    if t2.requires_grad:
        grad: Callable

        def grad(gradient: Tensor) -> Tensor:
            """
            Hold t1 constant and take the derivative. We get back 1, so we multiply by t1
            Args:
                gradient:
                    tensor data

            Returns:
            """
            return np.matmul(t1.data, grad)

        connections = [Connection(t1, grad)]
    return Tensor(data=data, requires_grad=t1.requires_grad or t2.requires_grad, connected_to=connections)