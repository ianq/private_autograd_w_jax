import jax.numpy as np
from typing import Optional, Callable
from autograd.nodes.tensor import Tensor, Connection


def addition_op(t1: Tensor, t2: Tensor) -> Tensor:
    """
    Addition of 2 tensors
    Args:
        t1: first tensor
        t2: second tensor

    Returns:
        New tensor which is the result of addition of previous
        2 tensors
    """
    assert t1.shape == t2.shape, "Try to add two matrices of different sizes"
    data_t1 = t1.data
    data_t2 = t2.data
    data_summed = data_t1 + data_t2

    def abstract_grad(t: Tensor) -> Callable:
        def grad(gradient: Tensor) -> Tensor:
            # We know that changing the value by 1 is equivalent to
            # changing the final gradient by 1. Thus, this is equivalent to
            # returning whatever was put in
            return gradient
        return grad

    connections = []
    if t1.requires_grad:
        connections.append(
            Connection(t1, abstract_grad(t1))
        )
    if t2.requires_grad:
        connections.append(
            Connection(t2, abstract_grad(t2))
        )

    return Tensor(
        data=data_summed,
        connected_to=connections,
        requires_grad=t1.requires_grad or t2.requires_grad
    )

